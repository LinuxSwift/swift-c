/* Swift for Linux SQLite3 C library example
Compile: swiftc airlite.swift -import-objc-header /usr/include/sqlite3.h -lsqlite3 -o airlite
Contributors: Armando I. Rivera (AIR) 2015-12-13
*/

import Foundation
import Glibc

func sqlColumnText(statement: COpaquePointer, column: Int32) -> String {
	return String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement,column)))!
}

func sqlVersion() -> String {
	let s = String.fromCString(sqlite3_libversion())!
	return "Sqlite3 Version: \(s)"
}

func sqlOpen(filename: String, database: UnsafeMutablePointer<COpaquePointer>) {
	let rc = sqlite3_open(filename, database)

	if (rc != SQLITE_OK) {
		print("Cannot open database " + String.fromCString(sqlite3_errmsg(database.memory))!)
		exit(1)
	}
}

func sqlExec(database: COpaquePointer, query: String) {
	var err_msg: UnsafeMutablePointer<CChar> = nil
	let rc = sqlite3_exec(database, query, nil, nil, &err_msg)

	if (rc != SQLITE_OK ) {
	        
	    print("SQL error: " + String.fromCString( err_msg)!)
	    
	    sqlite3_free(err_msg)      
	    sqlite3_close(database)
	    
	    exit(1)
	} 
}

func sqlPrepare(database: COpaquePointer, query: String, statement: UnsafeMutablePointer<COpaquePointer>) {
	let rc = sqlite3_prepare_v2(database,query,-1,statement,nil)

	if (rc != SQLITE_OK ) {
	    
	    print("Failed to select data")
	    print("SQL error: " + String.fromCString( sqlite3_errmsg(database))!)


	    sqlite3_close(database)
	    
	    exit(1)
	} 
}

func sqlStep(statement: COpaquePointer) -> Int32 {
	return sqlite3_step(statement)
}

func sqlClose(database: COpaquePointer){
	sqlite3_close(database)
}


var db: COpaquePointer = nil
var stmt: COpaquePointer = nil

print(sqlVersion(),"\n")

sqlOpen("test.db", database: &db)

var sql = 	"DROP TABLE IF EXISTS Cars;" +
            "CREATE TABLE Cars(Id INT, Name TEXT, Price INT);" +
            "INSERT INTO Cars VALUES(1, 'Audi', 52642);" +
            "INSERT INTO Cars VALUES(2, 'Mercedes', 57127);" +
            "INSERT INTO Cars VALUES(3, 'Skoda', 9000);" +
            "INSERT INTO Cars VALUES(4, 'Volvo', 29000);" +
            "INSERT INTO Cars VALUES(5, 'Bentley', 350000);" +
            "INSERT INTO Cars VALUES(6, 'Citroen', 21000);" +
            "INSERT INTO Cars VALUES(7, 'Hummer', 41400);" +
            "INSERT INTO Cars VALUES(8, 'Volkswagen', 21600);"

sqlExec(db, query: sql)


sql = "SELECT Name,Price FROM Cars Order By Price DESC"

sqlPrepare(db, query: sql, statement: &stmt)

while sqlStep(stmt) == SQLITE_ROW {
	var make  = sqlColumnText(stmt, column: 0)
	var price = sqlColumnText(stmt, column: 1)
	print(make,price)

}    


sqlClose(db)