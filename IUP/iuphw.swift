/*
Linux Swift IUP Hello World Example
Screen Shot: http://www.johnspikowski.com/Swift-C/swiftiuphello.png
Compile: swiftc iuphw.swift -import-objc-header /usr/include/iup/iup.h -liup -o iuphw
Contributors: John Spikowski, Armando I. Rivera (AIR) 2015-12-09
*/

import Foundation
import Glibc

func btn_exit_cb(ih: COpaquePointer) -> Int32 {
  return IUP_CLOSE
}
  
IupOpen(nil, nil)

var dlg = IupCreate("dialog")
var vbx = IupCreate("vbox")
var lbl = IupCreate("label")
var but = IupCreate("button")

IupStoreAttribute(dlg, "TITLE", "Swift IUP")
IupSetAttributes(vbx, "MARGIN=10x10, ALIGNMENT=ACENTER")
IupSetAttributes(lbl, "FONT=\"Arial, 24\",TITLE=\"Hello World\"")
IupStoreAttribute(but, "TITLE", " Quit ")

IupAppend(vbx, lbl)
IupAppend(vbx, but)
IupAppend(dlg, vbx)

IupSetCallback(but, "ACTION", btn_exit_cb)

IupShowXY(dlg, IUP_CENTER, IUP_CENTER)

IupMainLoop()

IupClose()
