/*
Linux Swift IUP LED Example
Screen Shot: http://www.johnspikowski.com/Swift-C/swift_iupled.png
Compile: swiftc iupled.swift -import-objc-header /usr/include/iup/iup.h -liup -o iupled
Contributors: John Spikowski 2015-12-11
*/


import Foundation
import Glibc

func btn_exit_cb(ih: COpaquePointer) -> Int32 {
  return IUP_CLOSE
}

IupOpen(nil, nil)

IupLoad("sample.led")

IupSetFunction("exit_dlg", btn_exit_cb)

IupShow(IupGetHandle("dlg"))

IupMainLoop()

IupClose()

